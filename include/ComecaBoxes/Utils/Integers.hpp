/**
 *   \file Integers.hpp
 *   \brief Contains utils functions for intergers manipulation
 *
 *  Contains:
 *    - std::uintmax_t concat<T>(const T& msb, const T& lsb);
 *
 */

#include <cstdint>     // For std::uintmax_t
#include <type_traits> // For std::is_integral

namespace comecaboxes {
namespace utils {
namespace integers {

/**
 * @brief Concatenates 2 integers using bits shift
 *
 * Simply returns the concatenation of 2 integers MSB & LSB by doing the
 * following: (MSB << sizeof(LSB) * 8) + LSB
 *
 * This function is available ONLY for intergral UNSIGNED types (MSBType &
 * LSBType) and when the sum of the sizes of the arguments types
 * (sizeof(MSBtype) + sizeof(LSBtype)) is at least of the same size as of
 * sizeof(std::uintmax_t). Otherwise, the code won't compile due to static
 * assertions.
 *
 *
 * @tparam MSBType The intergers type of the MSB
 * @tparam LSBType The intergers type of the LSB
 *
 * @param[in] msb The Most Significant Byte that will be shifted left
 * @param[in] lsb The Least Significant Byte
 * @return std::uintmax_t The integers containing the concatenation
 */
template <class MSBType, class LSBType>
constexpr std::uintmax_t concat(const MSBType &msb, const LSBType &lsb) {

  static_assert(std::is_integral<MSBType>::value &&
                    std::is_integral<LSBType>::value,
                "The input types must be INTEGRAL types (can't bit shift floats...)");

  static_assert(
      std::is_unsigned<MSBType>::value && std::is_unsigned<LSBType>::value,
      "The input types must be UNSIGNED types, as bit shift operation on "
      "signed type is undefined when the value is negative");

  static_assert(sizeof(std::uintmax_t) >= (sizeof(MSBType) + sizeof(LSBType)),
                "The input type used are too big and cannot be concatenated "
                "inside the std::uintmax_t");

  return (static_cast<std::uintmax_t>(msb) << (sizeof(LSBType) * 8)) +
         static_cast<std::uintmax_t>(lsb);
};

} // namespace integers
} // namespace utils
} // namespace comecaboxes
