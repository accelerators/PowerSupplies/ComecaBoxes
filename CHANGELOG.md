# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

## [6.0.0] - 22-09-2023

### Changed
* full rewrite ComecaBox Class
* improved numbers of call to the modbus
* fix state bug in ModbusStat all modbus. Comecabox statistics were ignored and replaced by statistics from the 3rd channel.

### Removed
* the reset command was removed because it was of no use.

## [5.0.0] - 06-10-2022 --------- FORGOTTEN TAG ---------

### Changed
* Class **ComecaBox**: [ACU-666](https://jira.esrf.fr/browse/ACU-666): Rename
  FirmwareVersion and BootLoaderVersion to IhmAppVersion and IhmBootVersion
  respectively and change their format ("MAJOR_MINOR")
* Class **ComecaBox**: Change the message displayed when alarm bit 29 is true

## [4.2.0] - 24-02-2022

### Fixed
* Class **ComecaBox**: Add default format 6.3f for Hourmeters attributes

### Removed
* Class **ComecaBox**: ARMLifetime attribute

## [4.1.1] - 16-07-2021

### Changed
* Class **ComecaBox**: Put Min (0.0) / Max ((2^32 -1) / 3600.) values for the
  Hourmeters Attributes

## [4.1.0] - 16-07-2021

### Added
* Class **ComecaBox**: Add 3 new attributes HourMeterRight, HourMeterMiddle,
  HourMeterLeft used to read/write the ARM registers at @ 111, 113 and 115
  respectively ([Ticket](https://jira.esrf.fr/browse/ACU-447))

## [4.0.2] - 18-03-2020

### Fixed
* Tango class **ModbusStat**: Forget error during DS startup in case of HSMAccess
DS not started (CS shutdown/restart case)

## [4.0.1] - 18-03-2020

### Changed
* Tango class **ModbusStat**: Attributes CallNumber, ErrNumber and LastReadCallNumber
are now polled by default (2000 ms period). Archive event for those attributes
also set by default

## [4.0.0] - 17-03-2021

### Added
* Tango class **ComecaBox**: Add new attribute DnsName with th rack ... DNS name

## [3.1.1] - 26-02-2021

### Fixed
* Tango class **ComecaBox**: Correctly remove useless code added for very
first release of Modbus stat!

## [3.1.0] - 18-02-2021

### Changed
* Tango class **ComecaBox**: Replace the TEST_BENCH compilation option
by a device property (default to false)

## [3.0.0] - 10/02/2021

### Added
* New tango class **ModbusStat**

### Fixed
* Tango class **ComecaBox**: Fix bug in ChannelDevNames attribute for
spare channel and cells 03 or 04

## [2.0.3] - 05/01/2021

### Fixed
* Class **ComecaBox**: Fix bug in ARMlifetime attribute (casting missing)

## [2.0.2] - 22-12-2020

### Added
* Class **ComecaBox**: Add some TL2 specific code

## [2.0.1] - 15-12-2020

### Fixed
* Class **ComecaBox**: Fix small bug in BootLoader version attribute

## [2.0.0] - 09-12-2020

### Added
* Class **ComecaBox**: Added two new attributea: BootLoaderVersion and
ARMLifeTime

## [1.1.10] - 08-07-2020

### Changed
* Class **ComecaBox**: New change in alarm messages

## [1.1.9] - 19-05-2020

### Changed
* Class **ComecaBox**: Changed alarm messages and  event messages in case
of alarm or alarm acknowledgment

## [1.1.8] - 27-01-2020

### Fixed
* Class **ComecaBox**: Fix bug which I hope was the reason of previous
release

## [1.1.7] - 17-01-2020

### Changed
* Class **ComecaBox**: Protect code against dhcp_com buffer being empty!
(Don't understand how this could happen!)

## [1.1.6] - 13-01-2020

### Changed
* Class **ComecaBox**: Force TimeZone to be CET (1) in init_device()
method

## [1.1.5] - 09-01-2020

### Changed
* Class **ComecaBox**: Protect code against date_data buffer being empty!
(Don't understand how this could happen!)

## [1.1.4] - 04-12-2019

### Changed
* Class **ComecaBox**: Throw exception in case of crazy number
found in Modbus for event number in EEPROM

## [1.1.3] - 29-11-2019

### Changed
* Class **ComecaBox**: Add missing possible case for the event 
registered in the Comeca IHM

## [1.1.2] - 14-11-2019

### Changed
* Class **MultiComecaBox**: Configure the archiving event for att 
BoxStates

## [1.1.1] - 25-09-2019

### Changed
* **ComecaBox** class: Due to a Comeca bug, the event number in rack as
to be considered as a unsinged value on 16 bits (unsigned short)

## [1.1.0] - 16-07-2019

### Changed
* **PoeHsc** class: Adpated to new software for Cisco switch
* **PoeHsc** class: Now support rack 12 at any place in cubicle
* **ComecaBox** class: Comment out state check in always_executed_hook() in order
to not latch error

## [1.0.1] - 03-06-2019

### Changed
* Rename spare PS for sextu/octu to only spare. This channel is not affected 
anymore (until more news)

## [1.0.0] - 03-06-2019

* Install in new EBS control system

## [0.2.1] - 22-01-2019

### Fixed
* Bug in HscPoe class to compute host name when cell number is
less than 10

## [0.2.0] - 15-10-2018

### Added
* A new Tango class **HscPoe** to ease GUI development
* ComecaBox class:
    * Date in status message when a communication problem with rack is detected
* MultiComecaBox class:
    * Add a Reset command propagating the command to all underneath devices
    
## [0.1.0] - 02-07-2018

### Added
* New Tango class (MultiComecaBox) to add a summary device (to ease GUI development)
* Two official ESRf files (README and CHANGELOG)
