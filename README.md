# ComecaBoxes

Tango classes to manage the EBS Comeca PS boxes. It contains three Tango classes which are:

* **ComecaBox** which controls one Comeca box (rack)
* **MultiComecaBox** with a single device which is a summary of the 11 Comeca box installed in one Hot Swap 
 Cubicle (HSC)
* **HscPoe** which controls the PoE for each comeca rack.
 
 Note that the ComecaBox Tango class uses the **Modbus** Tango class to communicate with the equipment but the Modbus class is managed in its own project

## Cloning

to clone this project, simply type:

```
git clone git@gitlab.esrf.fr:accelerators/PowerSupplies/ComecaBoxes.git
```

## Documentation

Pogo generated doc in **doc_html** folders of the two Tango classes in this project

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

Instructions on building the project.

CMake example: 

```bash
cd ComecaBoxes
mkdir -p build/debian9
cd build/debian9
cmake ../..
make
```