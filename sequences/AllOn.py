"""Sequences to switch ON all racks in one cubicle

This module is imported at macroserver startup and on "ReloadMacroLib"

"""

from __future__ import print_function

import time
import tango
import os

from sardana.macroserver.macro import Macro
from sardana.macroserver.macro import Type

_VERSION = 1.1

_DOMAIN_NAME = "srmag"
_PS_CABLING = [["qf6b","qf1a","qd2a"],
               ["qf8b","qd3a","qf4a"],
               ["qf8d","qf4b","qd5b"],
               ["qf6d","qd5d","qf4d"],
               ["qspare1","qf4e","qd3e"],
               ["qspare3","qd2e","qf1e"],
               ["qspare2","sd1b","sd1d"],
               ["sd1a","sd1e","qspare4"],
               ["sf2a","sf2e","of1b"],
               ["dq1b","dq2c","spare"],
               ["dq1d","dqspare","of1d"]]
#_PS_CABLING = [["qf6b","qf6w","qf6x"],
#               ["qf8b","qf8d","qf8x"],
#               ["qf6d","qf6y","qf6z"],
#               ["qf4a","qf4e","qf4x"],
#               ["of1b","of1w","of1x"],
#               ["qd3a","qd3e","qd3x"],
#               ["qd2a","qd2e","qd2x"],
#               ["sf2a","sf2e","sf2x"],
#               ["sd1a","sd1e","sd1x"],
#               ["of1d","of1y","of1z"],
#               ["dq1b","dq1d","dq1x"]]



class MacroBase(Macro):

    def build_dev_name(self,_rack,_cell,_nb):
        name = _DOMAIN_NAME + '/ps-'
        ps = _PS_CABLING[_rack][_nb]
        if _cell == 3:
            if ps == 'qd3e':
                ps = 'qf2e'
            elif ps == 'qf1e':
                ps = 'qd3e'
        if _cell == 4:
            if ps == 'qf1a':
                ps = 'qd3a'
            elif ps == 'qd3a':
                ps = 'qf2a'
        cell_str = "/c" + str(_cell).zfill(2)
        if len(ps) > 4 and ps[len(ps) - 1] != 'e':
            last_char = ps[len(ps) - 1]
            ps = ps[:-1]
            name = name + ps + cell_str + '-' + last_char
        elif len(ps) > 4 and ps.endswith('e'):
            name = name + ps + cell_str + "-1"
        else:
            last_char = ps[len(ps) - 1]
            ps = ps[:-1]
            name = name + ps + cell_str + '-' + last_char
        return name


    def check_group_reply(self,_replies):
        failed = False
        failed_devs = "On command failed for device(s) "
        failed_err = ""

        for reply in _replies:
            if reply.has_failed() is True:
                print("Failed reply")
                failed = True
                failed_devs = failed_devs + reply.dev_name() + ", "
                el = reply.get_err_stack()
                failed_err = failed_err + reply.dev_name() + ": " + el[0].desc + "\n"
        if failed is True:
            failed_devs = failed_devs[:-2] + "\n" + failed_err
            tango.Except.throw_exception("MacroServer",failed_devs,"MacroBase.check_group_reply")


# -------------------------------------------------------------------------------------
#
#                       VerticalOn sequence
#
# -------------------------------------------------------------------------------------


class VerticalOn(MacroBase):
    param_def = [["CellNb", Type.Integer, 37, "Cell number"],
                 ["DeltaT", Type.Float, 0.5, "Sleep time (in s) between command"]]

    def __init__(self, *args, **kwargs):
        super(VerticalOn, self).__init__(*args, **kwargs)
        
        self.left_names = []
        self.middle_names = []
        self.right_names = []


    def run(self, _cell, _delta):
        pr_str = "Cell {}: Starting macro VerticalOn".format(_cell)
        self.output(pr_str)
        del self.left_names[:]
        for rack in range(len(_PS_CABLING)):
            dev_name = self.build_dev_name(rack,_cell,0)
            self.left_names.append(dev_name)
        print(self.left_names)
        pr_str = "Cell {}: Switching ON all left channels".format(_cell)
        self.output(pr_str)
        self.column_on(self.left_names)

        time.sleep(_delta)

        del self.middle_names[:]
        for rack in range(len(_PS_CABLING)):
            dev_name = self.build_dev_name(rack,_cell,1)
            self.middle_names.append(dev_name)
        print(self.middle_names)
        pr_str = "Cell {}: Switching ON all middle channels".format(_cell)
        self.output(pr_str)
        self.column_on(self.middle_names)

        time.sleep(_delta)

        del self.right_names[:]
        for rack in range(len(_PS_CABLING)):
            dev_name = self.build_dev_name(rack,_cell,2)
            self.right_names.append(dev_name)

        if _cell == 3:
            self.right_names.append("srmag/ps-qf1/c03-e")
        elif _cell == 4:
            self.right_names.append("srmag/ps-qf1/c04-a")

        print(self.right_names)
        pr_str = "Cell {}: Switching ON all right channels".format(_cell)
        self.output(pr_str)
        self.column_on(self.right_names)
        pr_str = "Cell {}: VerticalOn macro end".format(_cell)
        self.output(pr_str)


    def column_on(self,_dev_names):
        grp1 = tango.Group("Column")
        for dev_name in _dev_names:
            grp1.add(dev_name)

        ans = grp1.read_attribute("connected")
        connecteds = [a.get_data().quality for a in ans]

        filtered_names = []
        for name, con in zip(_dev_names,connecteds):
            if con is True:
                filtered_names.append(name)

#        print('filtered_names length = ',len(filtered_names))
        if len(filtered_names) == 0:
            for name in _dev_names:
                if not 'spare' in name:
                    filtered_names.append(name)

#        print('filtered_names = ',filtered_names)

        grp2 = tango.Group("ColumnOn")
        for dev_name in filtered_names:
            grp2.add(dev_name)
 
        gcrs = grp2.command_inout("On")
        self.check_group_reply(gcrs)



# -------------------------------------------------------------------------------------
#
#                       HorizontalOn sequence
#
# -------------------------------------------------------------------------------------


class HorizontalOn(MacroBase):
    param_def = [["CellNb", Type.Integer, 37, "Cell number"],
                 ["DeltaT", Type.Float, 0.5, "Sleep time (in s) between command"]]

    def __init__(self, *args, **kwargs):
        super(HorizontalOn, self).__init__(*args, **kwargs)

    def run(self, _cell, _delta):
        pr_str = "Cell {}: Starting macro HorizontalOn".format(_cell)
        self.output(pr_str)
        dev_names_rack1 = []
        for chan in range(3):
            name = self.build_dev_name(0,_cell,chan)
            dev_names_rack1.append(name)

        if _cell == 3:
            dev_names_rack1.append("srmag/ps-qf1/c03-e")
        elif _cell == 4:
            dev_names_rack1.append("srmag/ps-qf1/c04-a")

        the_names = []
        for st in range(5):
            names = []
            start = 1 + (st * 2)
            for rack in range(start,start + 2):
                for chan in range(3):
                    dev_name = self.build_dev_name(rack,_cell,chan)
                    names.append(dev_name)
            the_names.append(names)
            print()

        grp = self.build_grp(dev_names_rack1)
        pr_str = "Cell {}: Switching ON the first rack (3 channels)".format(_cell)
        self.output(pr_str)
        gcrs = grp.command_inout("On")
        self.check_group_reply(gcrs)

        time.sleep(_delta)

        for names in the_names:
            grp = self.build_grp(names)
            pr_str = "Cell {}: Switching ON 2 racks (6 channels)".format(_cell)
            self.output(pr_str)
            gcrs = grp.command_inout("On")
            self.check_group_reply(gcrs)

            time.sleep(_delta)

        pr_str = "Cell {}: HorizontalOn macro end".format(_cell)
        self.output(pr_str)


    def build_grp(self,_dev_names):
        grp = tango.Group("g")
        for name in _dev_names:
            grp.add(name)

        ans = grp.read_attribute("connected")
        connecteds = [a.get_data().quality for a in ans]

        filtered_names = []
        for name, con in zip(_dev_names,connecteds):
            if con is True:
                filtered_names.append(name)

#        print('filtered_names length = ',len(filtered_names))
        if len(filtered_names) == 0:
            for name in _dev_names:
                if not 'spare' in name:
                    filtered_names.append(name)

#        print('filtered_names = ',filtered_names)
        the_grp = tango.Group("Hori_On")
        for name in filtered_names:
            the_grp.add(name)

        return the_grp



